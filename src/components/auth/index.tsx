import React, {useState} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import LoginPage from "./login";
import Register from "./register";
import  './style.scss'
import {Box} from "@mui/material";
import {instance} from "../../utilis/axios";
import {jsx} from "@emotion/react";
import JSX = jsx.JSX;
import {useAppDispatch} from "../../utilis/hook";
import {login} from "../../store/slice/auth";

const AuthRootComponent: React.FC = (): JSX.Element => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [repeatPassword, setRepeatPassword] = useState('')
    const dispatch = useAppDispatch()
    const navigate = useNavigate()

    const handSubmit = async (e:{preventDefault: ()=> void}) =>{
        e.preventDefault()
      if(location.pathname === '/login'){
          const userData = {
              email,
              password
          }
          const user = await instance.post('auth/login', userData)
          await dispatch(login(user.data))
          navigate('/')
      }else {
          const userData = {
              email,
              password
          }
          const newUser = await instance.post('auth/registration', userData)
          console.log(newUser)
      }

    }
    const location = useLocation();
    return(<div className="root">
            <form className="form" onSubmit={handSubmit}>
                <Box
                    component="div"
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    flexDirection="column"
                    maxWidth={6400}
                    margin='auto'
                    padding={5}
                    borderRadius={5}
                    boxShadow={'5px 5px 10px #ccc'}
                >
                    {location.pathname === '/login' ?
                        <LoginPage setEmail = {setEmail}
                                   setPassword = {setPassword}
                                   navigate = {navigate}
                        /> :
                        location.pathname === '/registration' ?
                            <Register setEmail = {setEmail}
                                      setPassword = {setPassword}
                                      setRepeatPassword = {setRepeatPassword}
                                      navigate={navigate}
                            /> :
                            null}
                </Box>
            </form>
    </div>
    )


}
export default AuthRootComponent