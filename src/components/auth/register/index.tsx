import React, {Fragment} from "react";
import {Button, TextField, Typography} from "@mui/material";
import {IPropsRegister} from "../../../common/types/auth";
import {jsx} from "@emotion/react";
import JSX = jsx.JSX;

const RegisterPage: React.FC<IPropsRegister> = (props: IPropsRegister ): JSX.Element =>{
    const {setEmail, setRepeatPassword,setPassword, navigate} = props
    return(
        <Fragment>
            <Typography variant="h3"  fontFamily='Poppins' textAlign='center'>Registration</Typography>
            <TextField fullWidth={true} margin='normal' label="Email" variant="outlined" placeholder="Enter your email"  onChange={(e) => setEmail(e.target.value)}/>
            <TextField type='password' fullWidth={true} margin='normal'  label="Password" variant="outlined" placeholder="Enter your password" onChange={(e) => setPassword(e.target.value)} />
            <TextField type='password' fullWidth={true} margin='normal'  label="Repeat the password" variant="outlined" placeholder="Enter your password"  onChange={(e) => setRepeatPassword(e.target.value)}/>
            <Button type='submit' sx={{fontFamily:'Poppins', marginTop:2, width:'60%',marginBottom:2}} variant="contained">Login</Button>
            <Typography variant="body1"  sx={{fontFamily:'Poppins'}}>Do you have an account?<span className='incitingText' onClick={()=>navigate('/login')}>Login</span></Typography>
        </Fragment>
    )
}

export default RegisterPage;