import React, {Fragment} from 'react';
import {TextField,Button,Typography} from "@mui/material";
import {IPropsLogin} from "../../../common/types/auth";
import {jsx} from "@emotion/react";
import JSX = jsx.JSX;

const LoginPage: React.FC<IPropsLogin> = (props: IPropsLogin): JSX.Element => {
    const {setPassword, setEmail, navigate} = props
    return(
        <Fragment>
            <Typography variant="h3"  fontFamily='Poppins' textAlign='center'>Authorization</Typography>
            <TextField fullWidth={true} margin='normal' label="Email" variant="outlined" placeholder="Enter your email" onChange={(e)=>setEmail(e.target.value)} />
            <TextField type='password' fullWidth={true} margin='normal'  label="Password" variant="outlined" placeholder="Enter your password"  onChange={(e)=>setPassword(e.target.value)}/>
            <Button type='submit' sx={{fontFamily:'Poppins', marginTop:2, width:'60%',marginBottom:2}} variant="contained">Login</Button>
            <Typography variant="body1"  sx={{fontFamily:'Poppins'}}>Do you have an account?<span className='incitingText' onClick={()=>navigate('/registration')}>Registration</span></Typography>
        </Fragment>
    )
}
export default LoginPage;