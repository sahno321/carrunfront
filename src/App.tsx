import React from 'react';
import Home from './components/home/index';
import {Route, Routes} from "react-router-dom";
import AuthRootComponent from "./components/auth";
import PrivateRoute from "./utilis/router/privateRoute";


function App() {
  return (
    <div className="App">
        <Routes>
            <Route element={<PrivateRoute/>}>
                <Route path="/" element={<Home/>} />
            </Route>
            <Route path="/login" element={<AuthRootComponent/>} />
            <Route path="/registration" element={<AuthRootComponent/>} />
        </Routes>
    </div>
  );
}

export default App;
