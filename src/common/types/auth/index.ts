import {NavigateFunction} from "react-router-dom";

export interface IPropsLogin {
    setPassword: (value: string) => void,
    setEmail: (value: string) => void,
    navigate: (to: string) => void,
}

export interface IPropsRegister {
    setPassword: (value: string) => void
    setRepeatPassword: (value: string) => void
    setEmail: (value: string) => void
    navigate: (to: string) => void

}

export interface IAuthState {
    user: {}
    isLogged: boolean
}